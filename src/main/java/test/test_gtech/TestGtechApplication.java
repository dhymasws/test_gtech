package test.test_gtech;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import test.test_gtech.service.CustomerService;
import test.test_gtech.service.TransactionService;

import javax.annotation.PostConstruct;

@SpringBootApplication
public class TestGtechApplication {

    @Autowired
    private CustomerService customerService;

    @Autowired
    private TransactionService transactionService;

    public static void main(String[] args) {
        SpringApplication.run(TestGtechApplication.class, args);

        System.out.println("Hello World");

    }

    //jawaban nomor 1.A

//    @PostConstruct
//    public void init() {
//        Long customerId = Long.valueOf(101);
//        Double amount = 100.0;
//
//        // Menambah saldo
//        customerService.increaseBalance(customerId, amount);
//
//        // Mengurangi saldo
//        customerService.decreaseBalance(customerId, amount);
//    }

    //jabawan nomor 1B dan 1C

//    @PostConstruct
//    public void init2(){
//        Long customerId = Long.valueOf(101);
//        String type = "top up";
//        Double amount = 115000.0;
//        transactionService.processTransaction(customerId, type, amount);
//    }

    @PostConstruct
    public void init2(){
        Long customerId = Long.valueOf(101);
        String type = "beli seblak";
        Double amount = 15000.0;
        transactionService.processTransaction(customerId, type, amount);
    }





}
