package test.test_gtech.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import test.test_gtech.entity.TransactionsEntity;

import java.util.Optional;

@Repository
public interface TransactionsRepository extends JpaRepository<TransactionsEntity, Long> {
    Optional<TransactionsEntity> findByIdAndTypeAndAmount(Long customerId, String type, Double amount);
}
