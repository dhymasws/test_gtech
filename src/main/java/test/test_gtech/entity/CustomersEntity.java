package test.test_gtech.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "customers")
@Data
@NoArgsConstructor

public class CustomersEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String name;

    @Column
    private String email;

    @Column
    private Double balance;

    public void increaseBalance(Double amount) {
        balance += amount;
    }

    public void decreaseBalance(Double amount) {
        balance -= amount;
    }

}
