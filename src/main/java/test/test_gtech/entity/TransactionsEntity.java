package test.test_gtech.entity;


import jdk.jfr.Timestamp;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "transactions")
@Data
@NoArgsConstructor
public class TransactionsEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "id_customer")
    private CustomersEntity customer;

    @Column
    private String type;

    @Column
    private Double amount;

    @Column
    @Timestamp
    private LocalDateTime created_at;
}
