package test.test_gtech.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import test.test_gtech.DTO.ResponseDTO;

@ControllerAdvice
public class GlobalException {
    @ExceptionHandler(value = Exception.class)
    public ResponseEntity<ResponseDTO<Object>> globalException(Exception e) {
        return new ResponseEntity<>(ResponseDTO.builder()
                .statusCode(HttpStatus.BAD_REQUEST.value())
                .message("Bad Request")
                .data(e.getMessage())
                .build(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = InsufficientBalanceException.class)
    public ResponseEntity<ResponseDTO<Object>>insufficientBalance(Exception e){
        return new ResponseEntity<>(ResponseDTO.builder()
                .statusCode(HttpStatus.BAD_REQUEST.value())
                .message("Insufficient Balance")
                .data(e.getMessage())
                .build(), HttpStatus.BAD_REQUEST);
    }

}
