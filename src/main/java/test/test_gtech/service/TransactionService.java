package test.test_gtech.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import test.test_gtech.entity.CustomersEntity;
import test.test_gtech.entity.TransactionsEntity;
import test.test_gtech.repository.TransactionsRepository;

import java.time.LocalDateTime;

@Service
public class TransactionService {

    @Autowired
    private CustomerService customerService;

    @Autowired
    private TransactionsRepository transactionRepository;

    public void processTransaction(Long customerId, String type, Double amount) {
        //Jawaban 1D
        // Cari transaksi dengan atribut yang sama
        boolean isDuplicateTransaction = transactionRepository
                .findByIdAndTypeAndAmount(customerId, type, amount)
                .isPresent();

        // Jika transaksi adalah duplikat, tidak perlu memprosesnya lagi
        if (isDuplicateTransaction) {
            return;
        }

        // Periksa jenis transaksi
        if (type.equalsIgnoreCase("top up") || type.equalsIgnoreCase("refund")) {
            // Jika transaksi adalah top up atau refund, tambahkan saldo
            customerService.increaseBalance(customerId, amount);
        } else {
            // Jika transaksi bukan top up atau refund, kurangi saldo
            customerService.decreaseBalance(customerId, amount);
        }

        // Simpan transaksi ke dalam database
        TransactionsEntity transaction = new TransactionsEntity();
        CustomersEntity customer = customerService.getCustomerById(customerId);
        transaction.setCustomer(customer);
        transaction.setType(type);
        transaction.setAmount(amount);
        transaction.setCreated_at(LocalDateTime.now());

        transactionRepository.save(transaction);
    }
}
