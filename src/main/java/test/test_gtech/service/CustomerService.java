package test.test_gtech.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import test.test_gtech.entity.CustomersEntity;
import test.test_gtech.exceptions.InsufficientBalanceException;
import test.test_gtech.exceptions.ResourceNotFoundException;
import test.test_gtech.repository.CustomersRepository;

import java.util.Optional;

@Service
public class CustomerService {

    @Autowired
    private CustomersRepository customersRepository;

    public void increaseBalance(Long customerId, Double amount) {
        CustomersEntity customer = customersRepository.findById(customerId)
                .orElseThrow(() -> new ResourceNotFoundException());

        customer.increaseBalance(amount);
        customersRepository.save(customer);
    }

    public void decreaseBalance(Long customerId, Double amount) {
        CustomersEntity customer = customersRepository.findById(customerId)
                .orElseThrow(() -> new ResourceNotFoundException());
        //Jawaban 1E. saldo tidak bisa minus
        if (customer.getBalance() < amount) {
            throw new InsufficientBalanceException();
        }

        customer.decreaseBalance(amount);
        customersRepository.save(customer);
    }

    public CustomersEntity getCustomerById(Long customerId) {
        Optional<CustomersEntity> customerOptional = customersRepository.findById(customerId);
        if (customerOptional.isEmpty()) {
            throw new ResourceNotFoundException();
        }
        return customerOptional.get();
    }
}

